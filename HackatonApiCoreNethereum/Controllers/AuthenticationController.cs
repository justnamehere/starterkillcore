using System.Threading.Tasks;
using HackatonApiCoreNethereum.Models.ViewModels.User;
using HackatonApiCoreNethereum.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HackatonApiCoreNethereum.Controllers{
    
    public class AuthenticationController : Controller{
        private readonly IUserService service;

        public AuthenticationController(IUserService service){
            this.service = service;
        }

        [HttpPost("/Auth")]
        public async Task<IActionResult> Authenticate([FromForm]LoginUserViewModel model){
            var token = await service.GetToken(model);
            if(token != null)
                return Json(token);            
            else
                ModelState.AddModelError("","Invalid username or password");

            return BadRequest(ModelState);                
        }        
    }
}