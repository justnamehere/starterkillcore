using System.Linq;
using System.Threading.Tasks;
using HackatonApiCoreNethereum.Models.Enums;
using HackatonApiCoreNethereum.Models.Identity.Context;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HackatonApiCoreNethereum.Models.Identity{
    public class DbInitializer{
        public static async Task Initialize(RoleManager<IdentityRole> roleManager){
            var adminRole = await roleManager.FindByNameAsync(Role.Admin.ToString());
            if(adminRole==null)
                await roleManager.CreateAsync(new IdentityRole(Role.Admin.ToString()));
            var userRole = await roleManager.FindByNameAsync(Role.User.ToString());
            if(userRole==null)
                await roleManager.CreateAsync(new IdentityRole(Role.User.ToString()));
            var moderatorRole = await roleManager.FindByNameAsync(Role.Moderator.ToString());
            if(moderatorRole==null)
                await roleManager.CreateAsync(new IdentityRole(Role.Moderator.ToString()));
        }
    }
}