using System.Collections.Generic;
using HackatonApiCoreNethereum.Models.Identity.Files;
using Microsoft.AspNetCore.Identity;

namespace HackatonApiCoreNethereum.Models.Identity.User
{
    public class ApplicationUser : IdentityUser{
        public string PublicAddress{get;set;}
        public string Organization{get;set;}
        public ProfileImage ProfileImage{get;set;}
    }
}