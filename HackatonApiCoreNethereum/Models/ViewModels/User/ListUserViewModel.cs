namespace HackatonApiCoreNethereum.Models.ViewModels.User{
    public class ListUserViewModel{

        public string Id{get;set;}
        public string Username { get; set; }
        public string PublicAddress { get; set; }
        public string ProfileImagePath {get;set;}
    }
}