﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HackatonApiCoreNethereum.Automapper;
using HackatonApiCoreNethereum.Models.Identity;
using HackatonApiCoreNethereum.Models.Identity.Context;
using HackatonApiCoreNethereum.Services;
using HackatonApiCoreNethereum.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using HackatonApiCoreNethereum.Models.Identity.User;
using Hangfire;

namespace HackatonApiCoreNethereum
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {            
            var connectionString = Configuration.GetConnectionString("Ethervent_Connection");
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));                        
            services.AddIdentity<ApplicationUser, IdentityRole>(x =>
            {
                x.User.RequireUniqueEmail = true;
            }).AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            services.AddAutoMapper(x => x.AddProfile<MappingProfile>());

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = Configuration["TokenConfiguration:ValidIssuer"],

                    ValidAudience = Configuration["TokenConfiguration:ValidAudience"],

                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["TokenConfiguration:SecurityKey"])),

                    ValidateIssuerSigningKey = true
                };
            });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                .RequireAuthenticatedUser()
                .Build();
            });

            services.AddTransient<IUserService, UserService>();           

            services.AddCors();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, RoleManager<IdentityRole> manager)
        {
            DbInitializer.Initialize(manager).Wait();

            app.UseStaticFiles();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }            

            app.UseAuthentication();
            
            app.UseMvc();
            app.UseCors(builder => builder.AllowAnyOrigin());
        }
    }
}
